/**
 * @file TaskCreate_1.c
 * @brief NEOS API test - task creation  
 * @description This tests the EOSTaskCreate function.
 *
 * @author Jerry Morrow
 *
 * @version 0.1
 *
 * Confidential, proprietary property of S&C Electric Co.
 * Copyright 2002, 2016 as an unpublished work.  All rights reserved.
 * No portion of this source code may be reproduced or distributed 
 * without the express written permission of S&C Electric Co.
 *	
 */
 
#include <stdio.h>
#include <sysexits.h>

#include "types.h"
#include "debug.h"
#include "eos.h"
#include <cmocka.h>

/* Task priority constants */
#define PRIORITY_NORMAL			(8)   /*!< Most/all standard tasks */

#define PREEMPT_TASK_ID 1 
#define NON_PREEMPT_TASK_ID 2 

boolean testResultPass1 = FALSE;  /*!< test result to indicate testTask has executed */ 

/**
 * @brief test task simply sleep for 100 msec, 5 times, and exit   
 * @param [in] - dummy unused pointer  
 * @retval none - unused pointer  
 */
	
void* testTask ( void* dummy )
{
	int i = 0;
	DEBUG_PRINT("testTask\n");

	while (i++ < 5){
		DEBUG_PRINT("testTask loop %i\n",i);
		EOSTaskSleep(100);
	}

   testResultPass1 = TRUE;

   return NULL;
}

/**
 * @brief Creates task with preemption flag set and checks execution 
 * @param [in] state unused pointer 
 * @retval none 
 */
	
static void test_EOSTaskCreate_Preemptive(void **state)
{
   HANDLE testTaskHandle;

	testTaskHandle = 
      EOSTaskCreate(&testTask,
                    0x300,
                    PRIORITY_NORMAL,
                    TASK_FLAGS_READY | TASK_FLAGS_PREEMPTIVE, 
                    NULL,
                    PREEMPT_TASK_ID);

   // assert and fail if testTaskHandle is NULL pointer
   assert_non_null(testTaskHandle);

   /* give task chance to execute */
   EOSTaskSleep(3000);

   /* check test result from task and reset flag  */ 
   assert_false ( testResultPass1 == FALSE);
   testResultPass1 = FALSE;

	return;
}

/**
 * @brief Creates task without preemption flag set and checks execution 
 * @param [in] state unused pointer 
 * @retval none 
 */
	
static void test_EOSTaskCreate_Non_Preemptive(void **state)
{
   HANDLE testTaskHandle;

   /* reset flag */
   testResultPass1 = FALSE;  

	testTaskHandle = 
      EOSTaskCreate( &testTask,
                     0x300,
                     PRIORITY_NORMAL,
                     TASK_FLAGS_READY, 
                     NULL,
                     NON_PREEMPT_TASK_ID);

   // assert and fail if testTaskHandle is NULL pointer
   assert_non_null(testTaskHandle);

   /* give task chance to execute */
   EOSTaskSleep(3000);

   /* check test result from task and reset flag  */ 
   assert_false ( testResultPass1 == FALSE);
   testResultPass1 = FALSE;
  
	return;
}

/**
 * @struct tests CMOCKA test list
 */ 
const struct CMUnitTest tests[] = {
	cmocka_unit_test(test_EOSTaskCreate_Preemptive),
	cmocka_unit_test(test_EOSTaskCreate_Non_Preemptive), 
};
 
/**
 * @brief main entry point
 * @param argc - number of command line arguments
 * @param argv - array of command line arguments
 * @return retval test result status used by CTest or other 
 *
 * @note The first command line argument (if provided) will be included in the output.)
 */

int main(int _argc, char** _argv)
{
   int retval=EX_OK; 

   DEBUG_OPEN();
	DEBUG_PRINT("cmocka_ex_test\n");

   retval = cmocka_run_group_tests(tests,NULL,NULL);

   DEBUG_CLOSE();

   /* kill child threads and end process, CTest will check return value */
   _exit(retval);
}
