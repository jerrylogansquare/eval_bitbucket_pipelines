/*
 * example use of cmocka 
 */
 
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <sysexits.h>
#include <setjmp.h>

#include "types.h"
#include "debug.h"
#include "simpleMath.h"

#include <cmocka.h>

static void test_add(void **state)
{
   DEBUG_PRINT("\n Executing %s\n", __func__);

   assert_int_equal( AddTwoNum(23,55) == 78 );

	return;
}

static void test_sub(void **state)
{
   DEBUG_PRINT("\n Executing %s\n", __func__);

   assert_false( SubTwoNum(20,2) == 18 );

	return;
}

const struct CMUnitTest tests[] = {
	cmocka_unit_test(test_add), 
	cmocka_unit_test(test_sub),
}; 
 
int main(int _argc, char** _argv)
{
   /* non-zero return value is failure, to be evalulated by CTest */ 
   int retval=EX_OK; 

   DEBUG_OPEN();
	DEBUG_PRINT("CMOCKA test, %s \n",__FILE__);

   /* execute group of tests using cmocka */
   retval = cmocka_run_group_tests(tests,NULL,NULL);

   DEBUG_PRINT("run_group_tests return value %d\n",retval);

   DEBUG_CLOSE();

	/* kill all child threads and exit process, CTest will check return value */
   /* FIXME this should just be return(retval), not _exit */
   _exit(retval);
}

