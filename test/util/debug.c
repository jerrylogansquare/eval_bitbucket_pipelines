/**
 * @file debug.c
 * @brief NEOS API source debug utility, file system available
 *
 * @author Jerry Morrow
 *
 * @version 0.1
 *
 * Confidential, proprietary property of S&C Electric Co.
 * Copyright 2002, 2016 as an unpublished work.  All rights reserved.
 * No portion of this source code may be reproduced or distributed 
 * without the express written permission of S&C Electric Co.
 *	
 */
 
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>

#define DEBUG_FILE_POSTFIX "_debug.out"

static FILE* fd = NULL;

/**
 * @brief Creates file for debug output  
 * @param [in] - fileName string
 * @retval none  
 */
void DebugOpen(char* fileName) 
{
   char debugFileName[NAME_MAX]="";  
  
   /* copy fileName to debugFileName up to the number of characters that will fit */  
   strncpy (debugFileName, fileName,(NAME_MAX - strlen(DEBUG_FILE_POSTFIX)) );

   /* postfix debug file extension */
   strcat (debugFileName, DEBUG_FILE_POSTFIX);

   /* create (and open) file for writing */
   fd = fopen(debugFileName,"w+");

   if(NULL == fd)
   {
      printf("\nD: fopen() Error!!!\n");
   }

   return;
}

/**
 * @brief Prints formatted message to file   
 * @param [in] - format, ...   
 * @retval none  
 */
void DebugPrint(const char* format, ...)
{
   va_list argptr;
   va_start(argptr, format);
   if(NULL != fd)
   {
      vfprintf(fd, format, argptr);
   }
   else
   {
      printf("\nD: fprintf () Error, fd is NULL\n");
   }
   va_end(argptr);
   return;
}

void DebugClose(void ) 
{
   if(NULL != fd)
   {
      fclose(fd); 
   }
   else
   {
      printf("\nD: fclose () Error, fd is NULL\n");
   }
   
   return;
}
/* EOF */
