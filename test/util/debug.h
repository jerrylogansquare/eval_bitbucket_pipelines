/**
 * @file debug.h
 *
 * @author Jerry Morrow (jmorrow) 
 * @version 0.1
 *
 * Confidential, proprietary property of S&C Electric Co.
 * Copyright 2002, 2016 as an unpublished work.  All rights reserved.
 * No portion of this source code may be reproduced or distributed 
 * without the express written permission of S&C Electric Co.
 *
 * @brief Debug output MACROS.   
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#ifdef DEBUG_FILE

void DebugPrint(const char* format, ...);
void DebugOpen(char* fileName); 
void DebugClose(void);

#warning "NOTE: Building with DEBUG_FILE conditional compile option set."

/*!< prints formatted string to file, when DEBUG_FILE is defined */
#define DEBUG_PRINT(...) do{ DebugPrint(__VA_ARGS__); } while (0)

/*!< creates debug log file, when DEBUG_FILE is defined */
#define DEBUG_OPEN(...)  do{ DebugOpen(__FILE__); } while (0)

/*!< closes debug log file, when DEBUG_FILE is defined */
#define DEBUG_CLOSE(...)  do{ DebugClose(); } while (0)

#elif DEBUG_STDERR

#warning "NOTE: Building with DEBUG_STDERR conditional compile option set."

/*!< prints formatted string to file, when DEBUG_STDERR is defined */
#define DEBUG_PRINT(...) do{ fprintf(stderr, __VA_ARGS__); } while (0)

/*!< prints formatted string to stderr, no open/close needed */
#define DEBUG_OPEN(...)  do{ } while (0)

/*!< prints formatted string to stderr, no open/close needed */
#define DEBUG_CLOSE(...)  do{ } while (0)

#else

/*!< 'does nothing', when DEBUG is not defined */
#define DEBUG_OPEN(...) do{ } while (0) 

/*!< 'does nothing', when DEBUG is not defined */
#define DEBUG_CLOSE(...) do{ } while (0) 

/*!< 'does nothing', when DEBUG is not defined */
#define DEBUG_PRINT(...) do{ } while (0) 

#endif /* DEBUG */

#endif /* DEBUG_H_ */
