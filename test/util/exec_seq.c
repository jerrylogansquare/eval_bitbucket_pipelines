/*
 * example use of cmocka for testing EOS task ready/block/yield 
 */
 
#include <stdio.h>

#include "types.h"
#include "debug.h"

/* execution sequence flags used for pass/fail determination */
static boolean execSeq[20];  /* will be initialized by ExecSeqReset */
static int execSeqIndex = 0; /* will be initialized by ExecSeqReset */

void ExecSeqReset(void) 
{
   unsigned int idx = 0;

   /* go through execution sequence flags */ 
   for ( idx=0; idx<sizeof(execSeq); idx++ )
   {
      execSeq[idx] = 0;     
   }

   execSeqIndex = 0;

   return;
}

void ExecSeqNext(unsigned int markerNo) 
{
   if ( execSeqIndex >= sizeof(execSeq) ) {
      DEBUG_PRINT("Out of Range index: %d\n", execSeqIndex);
   }
   else {
      DEBUG_PRINT("executing : %d, placing in: %d\n",markerNo,execSeqIndex);
      execSeq[execSeqIndex++] = markerNo;
   }
   return;
}

int ExecSeqCheck (int eesList[], unsigned int maxIndex) 
{
   unsigned int idx = 0;
   int result=0; /* default to PASS */ 

   if ( maxIndex > sizeof(execSeq) ) {
      DEBUG_PRINT("Out of Range index: %d\n",maxIndex);
      result=1;
   }
   else
   {
      /* go through execution sequence flags, make sure all are set to TRUE */ 
      for ( idx=0; idx < maxIndex; idx++ )
      {
         if ( execSeq[idx] != eesList[idx] ) {
            DEBUG_PRINT("Sequence failed at %d\n",idx);
            result=1;
            break;
         } 
      }
   }

   return (result); /* pass=0, fail=-1 */
}
