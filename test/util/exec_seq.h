/*
 *  execution sequence utility header
 */
 
void ExecSeqReset(void); 
void ExecSeqNext(unsigned int markerNo); 
int ExecSeqCheck (int eesList[], unsigned int maxIndex);

