/*
 * example use of cmocka for testing EOS scheduler sleep function  
 */
 
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#include "types.h"
#include "clock_mono.h"

double GetMonotonicClock(void)
{
    struct timespec time;

    if ( clock_gettime(CLOCK_MONOTONIC, &time) ){
        //  Simple handle of error by returning 0.0
        return (0.0);
    }
    return ((double)(time.tv_sec) + (double)(time.tv_nsec) * .000000001);
}

