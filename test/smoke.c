#include <stdio.h>
#include <sys/wait.h>
#include <sysexits.h>

#define LOG_LEVEL LOG_TRACE

#include "types.h"
#include "debug.h"
#include "eos.h"

/* Task priority constants */
#define PRIORITY_LOWEST			(2)		// Lowest priority
#define PRIORITY_BACKGROUND	(5)
#define PRIORITY_NORMAL			(8)		// Most/all standard tasks
#define PRIORITY_HIGH			(11)
#define PRIORITY_URGENT			(14)	// Highest application priority
#define PRIORITY_MAXIMUM		(15)	// For task timing analysis

int ret1=0;

pointer testTask(pointer dummy)
{
	int i = 0;
	DEBUG_PRINT("testTask\n");

	while (i++ < 3) {
		DEBUG_PRINT("testTask loop %i\n",i);
		EOSTaskSleep(500);
	}

   ret1 = 10; 

	return NULL;
}

HANDLE hndl = (HANDLE) NULL;

int main(int _argc, char** _argv)
{
   int retval = EX_OK; /* initialize return value to PASS */

   DEBUG_OPEN(); /* for debugging test */

	DEBUG_PRINT("smoke_test\n");

	/* Test task */
	hndl = EOSTaskCreate(&testTask,
                        0x300,
                        PRIORITY_NORMAL,
                        TASK_FLAGS_READY, 
                        NULL,
                        0x820); 

   /* set return value to 1 (FAIL) if handle is null */
   if (hndl == NULL)  
   { 
      retval = EX_OSERR; /* fail */ 
      DEBUG_PRINT("EOSTaskCreate did not return valid handle\n");
   }

   /* allow tasks to execute for 3 seconds */
   EOSTaskSleep(3000);

   if (ret1 != 10)
   { 
      retval = EX_OSERR; /* fail */
      DEBUG_PRINT("testTask did not complete execution\n");
   }

   DEBUG_CLOSE();

   /* application will not exit without call to _exit */ 
   /* FIXME this should just be return(retval), not _exit */
   _exit(retval);
}

