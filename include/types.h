/*******************************************************************************
 * Confidential, proprietary property of S&C Electric Co.
 * Copyright 2014 as an unpublished work.  All rights reserved.
 * No portion of this source code may be reproduced or distributed
 * without the express written permission of S&C Electric Co.
 *
 * Authors:
 * 	Oleg Nazarov <Oleg.Nazarov@sandc.ru>
 *
 *******************************************************************************/
#ifndef TYPES_H_
#define TYPES_H_

#define far

#define _PTR_ *

enum
{
#ifndef FALSE
	FALSE,
#ifndef TRUE
	TRUE,
#endif
#endif
};

/**
 * Constants
 */
enum
{
	RESULT_FAILURE = -128,//!< RESULT_FAILURE
	RESULT_LOCKED,        //!< RESULT_LOCKED
	RESULT_INTERRUPTED,   //!< RESULT_INTERRUPTED
	RESULT_IPS_ACCESS,    //!< RESULT_IPS_ACCESS
	RESULT_ACCESS_ERROR,  //!< RESULT_ACCESS_ERROR
	RESULT_ADDRESS_ERROR, //!< RESULT_ADDRESS_ERROR
	// legacy stuff
	/*	general busy flag */
	RESULT_BUSY,
	/*	illegal parameter passed to function call */
	RESULT_ILLEGAL_PARAMETER,
	/*	illegal state for the function call */
	RESULT_ILLEGAL_STATE,
	/* parameter is out of valid range */
	RESULT_PARAMETER_OUT_OF_RANGE,

	/** TCP result codes */

	/* socket timeout */
	RESULT_TCP_TIMEOUT,
	RESULT_TCP_SOCKET_WAIT_FOR_SHUTDOWN,
	RESULT_TCP_SOCKET_NOT_BOUND,
	RESULT_TCP_SOCKET_NOT_CONNECTED,
	RESULT_TCP_REMOTE_WINDOWFULL,
	RESULT_TCP_SOCKET_ADDRESS_IN_USE,
	/*	out of free sockets */
	RESULT_TCP_OUT_OF_SOCKETS,

	/** DNP result codes */

	/*	reroute the packet/message to remote station, activate socket */
	RESULT_DNP_DISPATCH_REPLY,
	/*	reroute the packet/message to remote station */
	RESULT_DNP_DISPATCH_REMOTE,
	/*	reroute the packet to local station */
	RESULT_DNP_DISPATCH_LOCAL,
	/*	reroute the packet to self address */
	RESULT_DNP_DISPATCH_SELF,
	/*	reroute the packet to broadcast station */
	RESULT_DNP_DISPATCH_BROADCAST,
	/*	reroute the packet to application at local station */
	RESULT_DNP_DISPATCH_APPLICATION,
	/*	reroute the packet to asynchronous Object 102 datasource */
	RESULT_DNP_DISPATCH_ASYNC,
	/*	application is misconfigured */
	RESULT_DNP_BAD_CONFIG,
	/* the packet successfully sent to UDP/IP stack */
	RESULT_DNP_SOCKET,
	/*	out of free packets */
	RESULT_DNP_OUT_OF_PACKETS,
	/*	out of free packets */
	RESULT_DNP_OUT_OF_SOCKETS,
	/*	out of free fragments */
	RESULT_DNP_OUT_OF_FRAGMENTS,
	/*	port does not exist */
	RESULT_DNP_BAD_PORT,
	/*	only a single task can operate with	a port */
	RESULT_DNP_CONCURRENT_ACCESS,
	/*	SO_TIMEOUT milliseconds	elapsed since receive()	*/
	RESULT_DNP_RX_TIMEOUT,
	/*	receive() is not fast enough */
	RESULT_DNP_RX_OVERRUN,
	/*	the operation completed, the socket does not have anything to send */
	RESULT_DNP_TX_COMPLETE,
	/* peer is not defined for current DNP over TCP connection/slot */
	RESULT_DNP_TCP_PEER_UNKNOWN,

	/** Socket result codes */

	/*	only SOCK_DGRAM are supported */
	RESULT_SOCKET_BAD_TYPE,
	/*	wrong option for this type of socket */
	RESULT_SOCKET_BAD_OPTION,
	/*	operation is in progress, no concurrent access allowed */
	RESULT_SOCKET_BUSY,
	/*	receive timed out */
	RESULT_SOCKET_TIMEOUT,
	/*	too many packets received (and dropped) */
	RESULT_SOCKET_RECEIVE_OVERRUN,
	/*	bind socket before use */
	RESULT_SOCKET_NOT_BOUND,
	/*	connect socket before use */
	RESULT_SOCKET_NOT_CONNECTED,
	/*	destination IP does not belong out network, no default gateway provided */
	RESULT_SOCKET_NO_ROUTE_TO_HOST,
	/*	destination IP cannot be resolved at ARP level */
	RESULT_SOCKET_DESTINATION_UNREACHABLE,
	/*	not enough resources, consider increasing buffers in attributes */
	RESULT_SOCKET_NO_PBUFFER,
	/*	not enough resources */
	RESULT_SOCKET_NO_NBUFFER,
	/*	number of sockets, receiving simultaneosly is limited */
	RESULT_SOCKET_NO_RECEIVE_SLOTS,
	/*	the destination IP cannot be sent to stopped interface */
	RESULT_SOCKET_NETWORK_INTERFACE_IS_DOWN,
	/*	though the packet is queued for sending, first we need to do ARP */
	RESULT_SOCKET_DEFERRED,
	/*	trying to send a packet that exceeds MTU size */
	RESULT_SOCKET_MTU_EXCEEDED,
	/*	only a single task can operate with	a socket */
	RESULT_SOCKET_CONCURRENT_ACCESS = RESULT_DNP_CONCURRENT_ACCESS,

	FS_ERROR_READ_SECTOR,			/* 0x01 */
	FS_ERROR_WRITE_SECTOR,			/* 0x02 */
	FS_ERROR_INCOMPATIBLE_DEVICE,	/* 0x03 */
	FS_ERROR_INVALID_MBR,			/* 0x04 */
	FS_ERROR_INCOMPATIBLE_FS,		/* 0x05 */
	FS_ERROR_DIRTY_FS,				/* 0x06 */
	FS_ERROR_BAD_FS_CHAIN,			/* 0x07 */
	FS_ERROR_INVALID_FAT_ENTRY,	/* 0x08 */
	FS_ERROR_END_OF_FILE,			/* 0x09 */
	FS_ERROR_FS_NOT_FOUND,			/* 0x0A */
	FS_ERROR_DRIVE_NOT_FOUND,		/* 0x0B */
	FS_ERROR_OTHER_DRIVE_OPENED,	/* 0x0C */
	FS_ERROR_DRIVE_ALREADY_OPENED,/* 0x0D */
	FS_ERROR_FS_NOT_OPENED,			/* 0x0E */
	FS_ERROR_FS_NOT_CLEANLY_CLOSED,/* 0x0F */
	FS_ERROR_MEDIA_CHANGED,			/* 0x10 */
	FS_ERROR_TOO_MANY_OPEN_FILES,	/* 0x11 */
	FS_ERROR_FILE_NOT_FOUND,		/* 0x12 */
	FS_ERROR_PATH_NOT_FOUND,		/* 0x13 */
	FS_ERROR_FILE_ALREADY_OPENED,	/* 0x14 */
	FS_ERROR_FILE_ALREADY_EXISTS,	/* 0x15 */
	FS_ERROR_NO_SUCH_FD,				/* 0x16 */
	FS_ERROR_FD_NOT_OPENED,			/* 0x17 */
	FS_ERROR_INVALID_FILENAME,		/* 0x18 */
	FS_ERROR_TOO_MANY_DIRECTORY_ENTRIES,/* 0x19 */
	FS_ERROR_DRIVE_FULL,				/* 0x1A */
	FS_ERROR_POSITION_OUT_OF_RANGE,	/* 0x1B */
	FS_ERROR_DIRECTORY_NOT_EMPTY,	/* 0x1C */
	FS_ERROR_FILE_IS_DIRECTORY,	/* 0x1D */
	FS_ERROR_FILE_IS_READONLY,		/* 0x1E */

	RESULT_TRANSMIT = -3, //!< RESULT_TRANSMIT
	RESULT_ASYNC = -2,    //!< RESULT_ASYNC
	RESULT_ERROR = -1,    //!< RESULT_ERROR
	RESULT_SUCCESS = 0,   //!< RESULT_SUCCESS

	FS_NO_ERROR = RESULT_SUCCESS,

	RESULT_DNP_DROP_PACKET = RESULT_SUCCESS,
};

#ifdef __DEBUG
/**
 * In debug mode all functions are global for better diagnostics of SIGSEGV-like crashes
 */
#define __static__
#else
#define __static__ static
#endif

#ifdef __x86_64__
#define __fast_call__ __attribute__((ms_abi))
#else
#define __fast_call__ __attribute__((fastcall))
#endif /* __x86_64__ */

typedef __INT8_TYPE__ int8;
typedef __UINT8_TYPE__ uint8;
typedef __INT16_TYPE__ int16;
typedef __UINT16_TYPE__ uint16;
typedef __INT32_TYPE__ int32;
typedef __UINT32_TYPE__ uint32;
typedef uint8 uint48[6];
typedef __INT64_TYPE__ __attribute__((aligned(1))) int64;
typedef __UINT64_TYPE__ __attribute__((aligned(1))) uint64;

#define MAX_INT8     (0x7F)
#define MAX_UINT8    (0xFF)
#define MAX_INT16    (0x7FFF)
#define MAX_UINT16   (0xFFFF)
#define MAX_INT32    (0x7FFFFFFFL)
#define MAX_UINT32   (0xFFFFFFFFUL)
#define MAX_INT64    (0x7FFFFFFFFFFFFFFFLL)
#define MAX_UINT64   (0xFFFFFFFFFFFFFFFFULL)

typedef volatile uint8 vuint8;
typedef volatile uint16 vuint16;
typedef volatile uint32 vuint32;
typedef volatile uint64 vuint64;
typedef volatile int8 vint8;
typedef volatile int16 vint16;
typedef volatile int32 vint32;
typedef volatile int64 vint64;

typedef void* pointer;
typedef uint8 boolean;

#define lengthof(x) (sizeof(x)/sizeof(x[0]))
#ifndef offsetof
#define offsetof(x, y) ((int)&(((x*)NULL)->y))
#endif
#define xstr(x) strx(x)
#define strx(x) #x

static inline int __clz(int x)
{
#if defined(__arm__)
	return 32 - __builtin_clz(x);
#elif defined(__aarch64__)
	return 32 - __builtin_clz(x);
#elif defined(__i386__)
	if(x)
	{
		return 32 - __builtin_clz(x);
	}
	return 0;
#elif defined(__x86_64__)
	if(x)
	{
		return 32 - __builtin_clz(x);
	}
	return 0;
#else
#error "Unsupported architecture"
#endif
}

static inline int __xadd(int* value, int n)
{
    __asm__ volatile (
        "xadd %0, (%1)\n\t"
        : "=r" (n)
        : "r" (value), "0" (n)
        : "memory", "cc");
    return n;
}

#define likely(x)      __builtin_expect(!!(x), 1)

#define unlikely(x)    __builtin_expect(!!(x), 0)

#define __xchg(x, y) __sync_lock_test_and_set(x, y)

#define __xchg16(x, y) __sync_lock_test_and_set(x, y)

#define __xchg64(x, y) __sync_lock_test_and_set(x, y)

static inline void __bit_shr(int* value, char count)
{
    __asm__ volatile (
		"shrl %0, (%1)\n\t"
        :
        : "c" (count), "r" (value)
        : "memory", "cc");
    return;
}

static inline char __bit_rcl(int* value)
{
	char old;
    __asm__ volatile (
		"stc\n\t"
		"rcll (%1)\n\t"
        "seto %0\n\t"
        : "=q" (old)
        : "r" (value)
        : "memory", "cc");
    return old;
}

static inline void __bit_set0(const void* value, int offset)
{
   __asm__ volatile (
        "bts %0, (%1)\n\t"
        :
        : "r" (offset), "r" (value)
        : "memory", "cc");
}

static inline void __bit_toggle0(const void* value, int offset)
{
   __asm__ volatile (
        "btc %0, (%1)\n\t"
        :
        : "r" (offset), "r" (value)
        : "memory", "cc");
}

static inline void __bit_clear0(const void* value, int offset)
{
   __asm__ volatile (
        "btr %0, (%1)\n\t"
        :
        : "r" (offset), "r" (value)
        : "memory", "cc");
}

static inline char __bit_set(const void* value, int offset)
{
	char old;
    __asm__ volatile (
        "bts %1, (%2)\n\t"
        "setc %0\n\t"
        : "=q" (old)
        : "r" (offset), "r" (value)
        : "memory", "cc");
    return old;
}

static inline char __bit_copy(const void* value, int offset, int offset1)
{
	char old;
    __asm__ volatile (
        "bt %2, (%1)\n\t"
    	"jc 1f\n\t"
        "btr %3, (%1)\n\t"
        "setc %0\n\t"
       	"jmp 2f\n\t"
    	"1: bts %3, (%1)\n\t"
        "setnc %0\n\t"
       	"2:\n\t"
        : "=q" (old)
        : "r" (value), "r" (offset), "r" (offset1)
        : "memory", "cc");
    return old;
}

static inline void __bit_test_and_set_if_0(const void* value, int offset, int offset1)
{
    __asm__ volatile (
        "bt %1, (%0)\n\t"
    	"jc 1f\n\t"
        "bts %2, (%0)\n\t"
       	"1:\n\t"
        :
        : "r" (value), "r" (offset), "r" (offset1)
        : "memory", "cc");
}

static inline void __bit_test_and_set_if_1(const void* value, int offset, int offset1)
{
    __asm__ volatile (
		"bt %1, (%0)\n\t"
		"jnc 1f\n\t"
		"bts %2, (%0)\n\t"
		"1:\n\t"
        :
        : "r" (value), "r" (offset), "r" (offset1)
        : "memory", "cc");
}

static inline char __bit_clear(const void* value, int offset)
{
	char old;
    __asm__ volatile (
        "btr %1, (%2)\n\t"
        "setc %0\n\t"
        : "=q" (old)
        : "r" (offset), "r" (value)
        : "memory", "cc");
    return old;
}

static inline void __sync_bit_lock(volatile void* memory, int offset)
{
    __asm__ volatile (
        "1: lock; bts %0, (%1)\n\t"
    	/* to improve the spin-lock performance */
		"pause\n\t"
		"jc 1b\n\t"
        :
        : "r" (offset), "r" (memory)
        : "memory", "cc");
}

static inline void __sync_bit_wait(volatile void* memory, int offset)
{
    __asm__ volatile (
        "1: bt %0, (%1)\n\t"
    	/* to improve the spin-lock performance */
		"pause\n\t"
		"jc 1b\n\t"
        :
        : "r" (offset), "r" (memory)
        : "memory", "cc");
}


static inline void __sync_bit_lock_and_yield(volatile void* memory, int offset)
{
    __asm__ volatile (
   		"jmp 2f\n\t"
		"1: call fiber_yield\n\t"
		"2: lock; bts %0, (%1)\n\t"
		"jc 1b\n\t"
        : : "r" (offset), "r" (memory) :
		  "ah", "ch", "dh",
		  "al", "cl", "dl",
		  "eax", "ecx", "edx",
		  "esi", "edi",
#ifdef __x86_64__
		  "rax", "rcx", "rdx",
		  "rsi", "rdi",			// these registers are actually preserved in Windows x86_64 ABI
		  "r8", "r9", "r10", "r11",
#endif /* __x86_64__ */
		  "memory", "cc");

}


static inline char __sync_bit_set(volatile void* memory, int offset)
{
	char old;
    __asm__ volatile (
        "lock; bts %1, (%2)\n\t"
        "setc %0\n\t"
        : "=q" (old)
        : "r" (offset), "r" (memory)
        : "memory", "cc");
    return old;
}

static inline void __sync_bit_set0(volatile void* memory, int offset)
{
    __asm__ volatile (
        "lock; bts %0, (%1)\n\t"
        :
        : "r" (offset), "r" (memory)
        : "memory", "cc");
}

static inline void __sync_bit_clear0(volatile void* memory, int offset)
{
    __asm__ volatile (
        "lock; btr %0, (%1)\n\t"
        :
        : "r" (offset), "r" (memory)
        : "memory", "cc");
}

static inline char __sync_bit_clear(volatile void* memory, int offset)
{
	char old;
    __asm__ volatile (
        "lock; btr %1, (%2)\n\t"
        "setc %0\n\t"
        : "=q" (old)
        : "r" (offset), "r" (memory)
        : "memory", "cc");
    return old;
}

static inline char __sync_bit_toggle(volatile void* memory, int offset)
{
	char old;
    __asm__ volatile (
        "lock; btc %1, (%2)\n\t"
        "setc %0\n\t"
        : "=q" (old)
        : "r" (offset), "r" (memory)
        : "memory", "cc");
    return old;
}

static inline char __sync_bit_test(volatile void* memory, int offset)
{
	char old;
    __asm__ volatile (
        "bt %1, (%2)\n\t"
        "setc %0\n\t"
        : "=q" (old)
        : "r" (offset), "r" (memory)
        : "cc");
    return old;
}

static inline void __sync_increment(volatile int* memory)
{
    __asm__ volatile (
        "lock; incl (%0)\n\t"
        :
        : "r" (memory)
        : "memory", "cc");
}

static inline char __sync_decrement(int* memory)
{
	char old;
    __asm__ volatile (
        "lock; dec (%1)\n\t"
        "setz %0\n\t"
        : "=q" (old)
        : "r" (memory)
        : "memory", "cc");
    return old;
}

/* this is x86 */
static inline uint64 rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64)hi << 32) | lo;
}


#pragma GCC diagnostic push
// we're going to ignore
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#pragma GCC diagnostic ignored "-Wuninitialized"

static inline int __bit_set_and_check(void* value, int offset)
{
	int result = -1;
	int result0; // ignore it being uninitialized with diagnostic pragmas around the function
    __asm__ volatile (
            "bts %2, (%3)\n\t"
			"jc 0f\n\t"
            "bsrl (%3), %1\n\t"
    		"cmovne %1, %0\n\t"
    		"sub $32, %0\n\t"
            "bsrl 4(%3), %1\n\t"
    		"cmovne %1, %0\n\t"
    		"add $32, %0\n\t"
    		"sub %2, %0\n\t"
			"0:"
        : "+r" (result)
        : "r" (result0), "r" (offset), "r" (value)
        : "memory", "cc");
    return result;
}

static inline int __bit_clear_and_check(void* value, int offset)
{
	int result = -1;
	int result0; // ignore it being uninitialized with diagnostic pragmas around the function
    __asm__ volatile (
            "btr %2, (%3)\n\t"
			"jnc 0f\n\t"
            "bsrl (%3), %1\n\t"
    		"cmovne %1, %0\n\t"
    		"sub $32, %0\n\t"
            "bsrl 4(%3), %1\n\t"
    		"cmovne %1, %0\n\t"
    		"add $32, %0\n\t"
			"0:"
        : "+r" (result)
        : "r" (result0), "r" (offset), "r" (value)
        : "memory", "cc");
    return result;
}

#pragma GCC diagnostic pop

static inline unsigned char __bit_unshuffle(unsigned short value)
{
    __asm__ volatile (
            "ror %0\n\t"
            "shr %0\n\t"
            "ror %0\n\t"
            "shr %0\n\t"
            "ror %0\n\t"
            "shr %0\n\t"
            "ror %0\n\t"
            "shr %0\n\t"
            "ror %0\n\t"
            "shr %0\n\t"
            "ror %0\n\t"
            "shr %0\n\t"
            "ror %0\n\t"
            "shr %0\n\t"
            "ror %0\n\t"
            "shr %0\n\t"
        : "+q" (value)
        :
        : "cc");
    return (char)value;
}


/**
 * used in postgresql type inet
 */
typedef struct _inet_struct
{
	uint8 FAMILY;
	uint8 MASK;
	uint16 SIZE;
	uint32 IP;
} INET_STRUCT;

#define __ntoh(x) (typeof(x))__builtin_choose_expr(sizeof(x)==sizeof(uint64), __builtin_bswap64(x), __builtin_bswap32(*((uint32*)((void*)&(x) - sizeof(uint32) + sizeof(x)))))

enum
{
	IP_ANY_ADDR
};

#endif /* TYPES_H_ */

/* EOF */
