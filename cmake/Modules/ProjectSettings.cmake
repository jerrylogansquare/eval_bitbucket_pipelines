# CXX flags
if(CMAKE_COMPILER_IS_GNUCXX)
    add_definitions(-Wall -Wno-deprecated -pthread -lang-c-c++-comments )
endif()

set(CMAKE_C_FLAGS "-std=gnu11")

# common defines
add_definitions( 
-D_GNU_SOURCE
)

# GNUInstallDirs - use GNU installation paths
# https://cmake.org/cmake/help/latest/module/GNUInstallDirs.html
#
# defines 
# CMAKE_INSTALL_<dir>
# CMAKE_INSTALL_FULL_<dir>
#
# where <dir> can be one of 
# BINDIR user executables (bin)
# SBINDIR system admin executables (sbin)
# LIBEXECDIR program executables (libexec)
# SYSCONFDIR read-only single-machine data (etc)
# SHAREDSTATEDIR modifiable architecture-independent data (com)
# LOCALSTATEDIR modifiable single-machine data (var)
# LIBDIR object code libraries (lib or lib64 or lib/<multiarch-tuple> on Debian)
# INCLUDEDIRC header files (include)
# OLDINCLUDEDIRC header files for non-gcc (/usr/include)
# DATAROOTDIR read-only architecture-independent data root (share)
# DATADIR read-only architecture-independent data (DATAROOTDIR)
# INFODIR info documentation (DATAROOTDIR/info)
# LOCALEDIR locale-dependent data (DATAROOTDIR/locale)
# MANDIR man documentation (DATAROOTDIR/man)
# DOCDIR documentation root (DATAROOTDIR/doc/PROJECT_NAME)
#
include(GNUInstallDirs)

set(CMAKE_INSTALL_DIR
"${CMAKE_INSTALL_LIBDIR}/cmake"
CACHE PATH "The subdirectory to install cmake config files")
    